document.addEventListener("DOMContentLoaded", () => {
  let newGame = true;
  let human = new Player("Player 1");
  let computer = new Player("com");
  let game = new Game();

  const humanChoices = document.querySelectorAll(".player__choices.human div");
  const comChoices = document.querySelectorAll(".player__choices.com div");
  const result = document.querySelector(".game__result");
  const vs = document.querySelector(".game__vs");
  const restart = document.querySelector(".restart");

  // tambah event listener ke pilihan pemain
  humanChoices.forEach((item) => {
    item.addEventListener("click", (event) => {
      if (newGame) {
        item.classList.add("selected");
        makeChoice(item.getAttribute("choice"));
      }
    });
  });

  restart.addEventListener("click", () => {
    if (!newGame) {
      restartGame();
    }
  });

  function toggleElements() {
    result.classList.toggle("show");
    vs.classList.toggle("show");
    restart.classList.toggle("show");
  }

  function makeChoice(choice) {
    if (newGame) {
      newGame = false;
      console.clear(); //? hapus atau comment jika tidak ingin clear log setiap restart game.

      // Assign pilihan ke Object human
      human.makeChoice(choice);
      console.log(`${human.getName()} memilih ${human.getChoice()}`);

      // Buat pilihan random untuk COM
      computer.getRandomChoice();
      document
        .querySelector(
          `.player__choices.com div[choice='${computer.getChoice()}']`
        )
        .classList.add("selected");
      console.log(`${computer.getName()} memilih ${computer.getChoice()}`);

      const winner = game.getWinner(human, computer);

      //jika PLAYER / COM menang
      if (typeof winner === "object") {
        result.innerHTML = `${winner.getName()} <br /> wins`;
        console.log(`${winner.getName()} wins`);
        console.table([human, computer], ["name", "points"]);
      }
      //jika DRAW
      else {
        result.innerHTML = "draw";
        console.log("draw");
      }

      toggleElements();
    }
  }

  function restartGame() {
    newGame = true;
    toggleElements();
    humanChoices.forEach((item) => {
      item.classList.remove("selected");
    });
    comChoices.forEach((item) => {
      item.classList.remove("selected");
    });
  }
});
