class Game {
  getWinner(human, computer) {
    var winner;
    if (
      (human.getChoice() === "batu" && computer.getChoice() === "kertas") ||
      (human.getChoice() === "kertas" && computer.getChoice() === "gunting") ||
      (human.getChoice() === "gunting" && computer.getChoice() === "batu")
    ) {
      computer.addPoints();
      winner = computer;
    } else if (
      (computer.getChoice() === "batu" && human.getChoice() === "kertas") ||
      (computer.getChoice() === "kertas" && human.getChoice() === "gunting") ||
      (computer.getChoice() === "gunting" && human.getChoice() === "batu")
    ) {
      human.addPoints();
      winner = human;
    } else {
      winner = "draw";
    }
    return winner;
  }
}
